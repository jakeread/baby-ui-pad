## HTMAA 2021 Example Project

:point_right: :point_left:  

A demo project! I'll keep documentation for this thing [here on the MTM site](https://mtm.cba.mit.edu/2021/2021-10_htmaa-demo),  but this repo will store design files / code, etc. Should be updated throughout the term. 

### Circuit Design / ECAD 

[files in /circuit](/circuit) and [the fab lab Eagle Library is here](https://gitlab.fabcloud.org/pub/libraries/electronics/eagle/)  

![schem](log/2021-10-07_pad-schem.png)
![route](log/2021-10-07_pad-routed.png)