## 2021 10 07 

Haven't kept decent notes for this, but will start. Plan is to do one 'complete' project, though not *really* appropriate as a final project, just a demo of how it all comes together: a little circuit, a little firmware, a little interface / application code, and some 3D Printing, as a treat. 

I am also curious to drive some OSAP work for multiple circuits / boards on USB links, re-assembling intelligently into modular programs. Will use this to check that out. 

